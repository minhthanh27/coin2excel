const axios = require('axios');

class CoinMarketCapModel {
  constructor() {
    this.baseUrl = 'https://api.coinmarketcap.com/v1';
  }

  /**
   * Get all cryptocurrency market
   * 
   * @param {object} opt 
   * @returns Promise
   * @memberof CoinMarketCapModel
   * @description { convert: EUR, limit:10 }
   */
  tickers(opt) {
    return axios.get(this.baseUrl + '/ticker', {
      params: opt,
    }).then(res => res.data);
  }

  
  /**
   * Get a special cryptocurrency market
   * 
   * @param {string} currency 
   * @param {object} opt 
   * @returns Promise
   * @memberof CoinMarketCapModel
   * @description currency="bitcoin"; opt = { convert: EUR, limit:10 } 
   */
  ticker(currency, opt) {
    return axios.get(this.baseUrl + '/ticker/' + currency.toLowerCase(), {
      params: opt,
    }).then(res => res.data);
  }

  /**
   * Get global market information
   * 
   * @param {object} opt
   * @returns Promise
   * @memberof CoinMarketCapModel
   * @description { convert: EUR, limit:10 }
   */
  global(opt) {
    return axios.get(this.baseUrl + '/global', {
      params: opt
    });
  }
}

module.exports = new CoinMarketCapModel();
