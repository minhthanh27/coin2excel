const fs = require('fs');
const moment = require('moment');
const CoinMarket = require('./CoinMarket');
const XlsxTemplate = require('xlsx-template');
const templateFile = fs.readFileSync('./Template.xlsx')


// Create a template
const template = new XlsxTemplate(templateFile);

function generateReport() {
    const now = moment().format('h:mmA DD-MM-YYYY')
    const filePath = `${__dirname}/reports/${now}.xlsx`;
    CoinMarket.tickers({limit: 100}).then((data) => {
        const values = {
            date: moment().format('DD/MM/YYYY'),
            coin: data.map((_) => {
                return {
                    rank: _.rank,
                    name:`${_.name} (${_.id})`,
                    price_usd: _.price_usd,
                    price_btc: _.price_btc,
                    '24h_volume': _['24h_volume_usd'],
                    market_cap: _.market_cap_usd,
                    available_supply: _.available_supply,
                    total_supply: _.total_supply,
                    max_supply: _.max_supply,
                    percent_change_1h: _.percent_change_1h,
                    percent_change_24h: _.percent_change_24h,
                    percent_change_7d: _.percent_change_7d,
                    last_update: moment(new Date(parseInt(_.last_updated)*1000))
                }
            })
        }
        // Perform substitution
        template.substitute(1, values);
        // Get binary data
        const output = template.generate({ type: 'nodebuffer' });
        
        // Save report
        fs.writeFileSync(filePath, output);
        console.log (`Generated report at ${now}`);
        // res.download(filePath, `Bookings ${moment(month, 'YYYYMM').format('MMM YYYY')}.xlsx`);
    })
    .catch(console.error);
}

setInterval(generateReport, 1000*60);